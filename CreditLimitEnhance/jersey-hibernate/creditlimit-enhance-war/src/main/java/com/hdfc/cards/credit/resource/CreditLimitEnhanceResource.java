package com.hdfc.cards.credit.resource;

import com.hdfc.cards.credit.offers.exception.*;
import com.hdfc.cards.credit.offers.service.CreditLImitEnhanceServiceI;
import com.hdfc.cards.credit.offers.service.CreditLimitEnhanceServiceImpl;
import com.hdfc.cards.credit.offers.service.dto.ClePromocodeServiceResponse;
import com.hdfc.cards.credit.utils.CleValidator;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/credit-limit-enhance")
public class CreditLimitEnhanceResource {

    private static final String healthCheckMessage = "creditlimit-enhance-service is running fine...";
    private CreditLImitEnhanceServiceI creditLImitEnhanceServiceI = null;
    private CleValidator cleValidator = null;
    private static final Logger resourLogger = Logger.getLogger(CreditLimitEnhanceResource.class);

    @Path("/healthCheck")
    @Produces(MediaType.TEXT_HTML)
    @GET
    public Response healthCheck() {

        return Response.status(200).entity(healthCheckMessage).build();

    }

    @Path("/verify")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes(MediaType.APPLICATION_JSON)
    @GET
    public Response verifyPromocode(@QueryParam("promocode") String promocode) throws BusinessException, CleDataAccessException, SystemException {

        resourLogger.debug("entered into resource layer : " + promocode);
        //valide the promocode
        // whether it is empty or its null or valid length
        cleValidator = new CleValidator();
        cleValidator.validate(promocode);
        ClePromocodeServiceResponse clePromocodeServiceResponse = null;
        creditLImitEnhanceServiceI = new CreditLimitEnhanceServiceImpl();
        clePromocodeServiceResponse = creditLImitEnhanceServiceI.verifyPromocode(promocode);
        resourLogger.info("response from service .." + clePromocodeServiceResponse);

        resourLogger.debug("exiting from resource layer with success response");
        return Response.status(200).entity(clePromocodeServiceResponse).build();


    }

    public static void main(String[] args) {
		/*CreditLimitEnhanceResource creditLimitEnhanceResource = new CreditLimitEnhanceResource();
		Response serviceResponse = creditLimitEnhanceResource.verifyPromocode("hdfc2020");
		if (serviceResponse.getEntity() instanceof CleErrorResponse) {
			CleErrorResponse cleErrorResponse = (CleErrorResponse) serviceResponse.getEntity();
			System.out.println(cleErrorResponse.getErrorCode());
			System.out.println(cleErrorResponse.getErrorMessage());
		}
		if (serviceResponse.getEntity() instanceof ClePromocodeServiceResponse) {
			ClePromocodeServiceResponse clePromocodeServiceResponse = (ClePromocodeServiceResponse) serviceResponse.getEntity();
			System.out.println(clePromocodeServiceResponse);


		}
*/
    }
}
