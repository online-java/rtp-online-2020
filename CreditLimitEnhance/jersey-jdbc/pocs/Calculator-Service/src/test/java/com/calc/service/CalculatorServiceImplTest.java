package com.calc.service;

import org.junit.*;

public class CalculatorServiceImplTest {
   private  CalculatorServiceImpl calculatorService;

//setUpThe environment
    @Before
    public void setUp(){
         calculatorService = new CalculatorServiceImpl();

        System.out.println("@Before called");

    }


    @Test
    public void testAdd(){

        int expectedResult;
        expectedResult = 15;
        int result = calculatorService.add(5, 10);

        Assert.assertEquals(expectedResult, result);
        System.out.println("testing completed");
    }
    @Test
    public void testDivision(){

        int a = 15;
        int b = 5;
        int result = calculatorService.division(a, b);

        int expected = 3;
        Assert.assertEquals(expected, result);
        /*  System.out.println(result);*/
        /**
         * alt+ctrl+i+o+l
         */

    }

    @Test
    //@Test(expected = ArithmeticException.class)
    public void testDivisionWithWrongInput() {

        int a = 15;
        int b = 5;
        int result = calculatorService.division(a, b);

        int expected = 3;
        Assert.assertEquals(expected, result);
        /*  System.out.println(result);*/
        /**
         * alt+ctrl+i+o+l
         */

    }
    @After
    public void tearDown(){

        calculatorService = null;
    }




}
