package com.flipkrt.orders.service;

import com.flipkrt.orders.beans.Order;
import com.flipkrt.orders.dao.OrderDao;
import com.flipkrt.orders.dao.OrderDaoImpl;

import java.sql.SQLException;

public class OrderServiceImpl implements OrderService
{
    private OrderDao orderDao = new OrderDaoImpl();

    @Override
    public String getAllOrders () throws SQLException
    {
        orderDao.getAllOrders();
        return null;
    }

    @Override
    public String createOrder (Order order) throws SQLException, ClassNotFoundException
    {
        return orderDao.createOrder(order);
    }

    @Override
    public Order getOrder (Integer id) throws SQLException, ClassNotFoundException
    {
        orderDao.getOrder(id);
        return null;
    }

    @Override
    public String updateOrder (Order order) throws SQLException, ClassNotFoundException
    {
        orderDao.updateOrder(order);
        return null;
    }

    @Override
    public String deleteOrder (Integer id) throws SQLException
    {
        orderDao.deleteOrder(id);
        return null;
    }
}
