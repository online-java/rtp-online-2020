package com.flipkrt.orders.resource;

import com.flipkrt.orders.beans.Order;
import com.flipkrt.orders.service.OrderService;
import com.flipkrt.orders.service.OrderServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;

@Path("/order-service")
public class OrderResource
{

    @Path("/check")
    @GET
    @Produces("text/html")
    public String Check ()
    {
        String status = "order service is working fine.....";
        return status;
    }

    @Path("/createOrder")
    @POST
    @Consumes({"application/xml", "application/json"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, "text/html"})
    public String createOrder (Order orders) throws SQLException, ClassNotFoundException
    {
        OrderService orderService = new OrderServiceImpl();
        orderService.createOrder(orders);
        String status = "order created successfully";
        return status;
    }

    @Path("/getallorders")
    @GET
    @Produces("text/html")
    public String getAllorders () throws SQLException
    {
        OrderService orderService = new OrderServiceImpl();
        orderService.getAllOrders();
        return "all order printed successfully";
    }

    @Path("/getOrder")
    @POST
    @Consumes({"application/xml", "application/json", "text/html"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, "text/html"})
    public String getOrder (Order order) throws SQLException, ClassNotFoundException
    {
        OrderService orderService = new OrderServiceImpl();
        orderService.getOrder(order.getId());
        String status = "order found";
        return status;
    }

    @Path("/deleteorder")
    @POST
    @Consumes({"application/xml", "application/json", "text/html"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, "text/html"})
    public String deleteorders (Order order) throws SQLException
    {
        OrderService orderService = new OrderServiceImpl();
        orderService.deleteOrder(order.getId());
        return "Order Deleted successfully";
    }

    @Path("/updateOrder")
    @POST
    @Consumes({"application/xml", "application/json", "text/html"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, "text/html"})
    public String updateOrder (Order order) throws SQLException, ClassNotFoundException
    {
        OrderService orderService = new OrderServiceImpl();
        orderService.updateOrder(order);
        String status = "Order Updated Successfully ";
        return status;
    }
}

