package com.flipkrt.orders.entity;

import javax.persistence.*;

@Entity
@Table(name= "`order`")
public class OrderEntity {
    @Id
    @Column(name="`id`")
    private Integer id;

    @Column(name="`orderName`")
    private String orderName;
    @Column(name="`deliveryAddress`")
    private String deliveryAddress;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }
    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }
}
