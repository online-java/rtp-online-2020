package com.hdfc.cards.credit.offers.dao.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ClePromocodeDaoResponse {

    private Double  currentLimit;
    private Double eligibleAmount;
    private String expDate;
    private String promocode;

    public Double getCurrentLimit() {
        return currentLimit;
    }

    public void setCurrentLimit(Double currentLimit) {
        this.currentLimit = currentLimit;
    }

    public Double getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(Double eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    @Override
    public String toString() {
        return "ClePromocodeDaoResponse{" +
                "currentLimit=" + currentLimit +
                ", eligibleAmount=" + eligibleAmount +
                ", expDate='" + expDate + '\'' +
                ", promocode='" + promocode + '\'' +
                '}';
    }
}
