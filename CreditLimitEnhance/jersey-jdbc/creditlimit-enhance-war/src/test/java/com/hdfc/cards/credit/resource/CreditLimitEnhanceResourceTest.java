package com.hdfc.cards.credit.resource;

import com.hdfc.cards.credit.offers.exception.BusinessException;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.CleEnum;
import com.hdfc.cards.credit.offers.exception.SystemException;
import com.hdfc.cards.credit.offers.service.CreditLImitEnhanceServiceI;
import com.hdfc.cards.credit.offers.service.CreditLimitEnhanceServiceImpl;
import com.hdfc.cards.credit.offers.service.dto.ClePromocodeServiceResponse;
import com.hdfc.cards.credit.utils.CleValidator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.ws.rs.core.Response;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CreditLimitEnhanceResource.class)
public class CreditLimitEnhanceResourceTest {
    private CreditLimitEnhanceResource creditLimitEnhanceResource = null;
    private CreditLImitEnhanceServiceI creditLImitEnhanceServiceImplMock = null;
    private CleValidator cleValidatorMock = null;

    @Before
    public void setUp() {
        creditLimitEnhanceResource = new CreditLimitEnhanceResource();
        creditLImitEnhanceServiceImplMock = PowerMockito.mock(CreditLimitEnhanceServiceImpl.class);
        cleValidatorMock = PowerMockito.mock(CleValidator.class);
    }

    @Test
    public void verifyPromocode() throws Exception {
        PowerMockito.whenNew(CreditLimitEnhanceServiceImpl.class).withNoArguments().thenReturn((CreditLimitEnhanceServiceImpl) creditLImitEnhanceServiceImplMock);
        PowerMockito.doNothing().when(cleValidatorMock).validate(Mockito.anyString());
        cleValidatorMock.validate("hdfc2020");
        PowerMockito.when(creditLImitEnhanceServiceImplMock.verifyPromocode("HDFC2020")).thenReturn(prepareRespone());
        Response response = creditLimitEnhanceResource.verifyPromocode("HDFC2020");
        ClePromocodeServiceResponse clePromocodeServiceResponse1 = (ClePromocodeServiceResponse) response.getEntity();
        Assert.assertEquals(5000d, (double) clePromocodeServiceResponse1.getCurrentLimit(), 1);
        Assert.assertEquals(1000, clePromocodeServiceResponse1.getEligibleAmount(), 1);
        Assert.assertEquals("", clePromocodeServiceResponse1.getExpDate());
        Assert.assertEquals(200, response.getStatus());
    }

    /**
     * this test method is used to test whether it throws business exception if invalid promocode is passed
     */
    @Test(expected = CleDataAccessException.class)
    public void verifyPromocodeWithInvlidPromocode() throws Exception {
        PowerMockito.whenNew(CreditLimitEnhanceServiceImpl.class).withNoArguments().thenReturn((CreditLimitEnhanceServiceImpl) creditLImitEnhanceServiceImplMock);
        PowerMockito.doNothing().when(cleValidatorMock).validate(Mockito.anyString());
        cleValidatorMock.validate("hdfc2021");
        PowerMockito.when(creditLImitEnhanceServiceImplMock.verifyPromocode("HDFC2021")).thenThrow(new CleDataAccessException(CleEnum.PROMOCODE_DETAILS_NOT_FOUND));
        creditLimitEnhanceResource.verifyPromocode("HDFC2021");


    }

    private ClePromocodeServiceResponse prepareRespone() {
        ClePromocodeServiceResponse clePromocodeServiceResponse = new ClePromocodeServiceResponse();
        clePromocodeServiceResponse.setCurrentLimit(5000d);
        clePromocodeServiceResponse.setEligibleAmount(1000d);
        clePromocodeServiceResponse.setExpDate("");
        clePromocodeServiceResponse.setPromocode("HDFC2020");
        return clePromocodeServiceResponse;
    }

    @After
    public void tearDown() {
        creditLimitEnhanceResource = null;
        cleValidatorMock = null;
        creditLImitEnhanceServiceImplMock = null;
    }
}