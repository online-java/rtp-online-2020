package com.hdfc.cards.credit.offers.exception;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class CleGloabalExceptionHandlerTest {
    CleGloabalExceptionHandler cleGloabalExceptionHandler;

    @Before
    public void setup() {
        cleGloabalExceptionHandler = new CleGloabalExceptionHandler();
    }

    @Test
    public void toResponseWithBusinessException() {
        BusinessException businessException = new BusinessException(CleEnum.PROMOCODE_IS_NOT_VALID);
        String expmsg = "promocode is not valid";
        String expcode = "CLE1001";
        Response response = cleGloabalExceptionHandler.toResponse(businessException);
        CleErrorResponse cleErrorResponse = (CleErrorResponse) response.getEntity();
        assertEquals(expmsg, cleErrorResponse.getErrorMessage());
        assertEquals(expcode, cleErrorResponse.getErrorCode());
        assertEquals(409, response.getStatus());
    }

    @Test
    public void toResponseWithCleDataAccessException() {

        CleDataAccessException cleDataAccessException = new CleDataAccessException(CleEnum.PROMOCODE_DETAILS_NOT_FOUND);
        String expmsg = "no data found for given promocode";
        String expcode = "CLE1002";
        Response response = cleGloabalExceptionHandler.toResponse(cleDataAccessException);
        CleErrorResponse cleErrorResponse = (CleErrorResponse) response.getEntity();
        assertEquals(expmsg, cleErrorResponse.getErrorMessage());
        assertEquals(expcode, cleErrorResponse.getErrorCode());
        assertEquals(409, response.getStatus());
    }

    @Test
    public void toResponseWithSystemException() {
        SystemException systemException = new SystemException(CleEnum.INTERNAL_SERVER_ERROR);
        String expmsg = "something went wrong";
        String expcode = "CLE1003";
        Response response = cleGloabalExceptionHandler.toResponse(systemException);
        CleErrorResponse cleErrorResponse = (CleErrorResponse) response.getEntity();
        assertEquals(expmsg, cleErrorResponse.getErrorMessage());
        assertEquals(expcode, cleErrorResponse.getErrorCode());
        assertEquals(500, response.getStatus());
    }

    @Test
    public void toResponseWithNullPointerException() {
        SystemException systemException = new SystemException(CleEnum.INTERNAL_SERVER_ERROR);
        String expmsg = "something went wrong";
        String expcode = "CLE1003";
        Response response = cleGloabalExceptionHandler.toResponse(systemException);
        CleErrorResponse cleErrorResponse = (CleErrorResponse) response.getEntity();
        assertEquals(expmsg, cleErrorResponse.getErrorMessage());
        assertEquals(expcode, cleErrorResponse.getErrorCode());
        assertEquals(500, response.getStatus());
    }

    @Test
    public void toResponseWithUnCatchException() {
        // any unknown Exception raised for example
        Exception exception = new ArithmeticException();
        String expmsg = "something went wrong";
        String expcode = "CLE1003";
        Response response = cleGloabalExceptionHandler.toResponse(exception);
        CleErrorResponse cleErrorResponse = (CleErrorResponse) response.getEntity();
        assertEquals(expmsg, cleErrorResponse.getErrorMessage());
        assertEquals(expcode, cleErrorResponse.getErrorCode());
        assertEquals(500, response.getStatus());
    }

    @After
    public void tearDown() {
        cleGloabalExceptionHandler = null;
    }
}