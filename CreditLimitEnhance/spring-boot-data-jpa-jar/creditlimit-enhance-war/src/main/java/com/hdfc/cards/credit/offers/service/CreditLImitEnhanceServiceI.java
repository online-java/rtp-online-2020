package com.hdfc.cards.credit.offers.service;


import com.hdfc.cards.credit.offers.dto.ClePromocodeServiceResponse;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.SystemException;

public interface CreditLImitEnhanceServiceI {

    public ClePromocodeServiceResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException;

}
