package com.hdfc.cards.credit.offers.service;

import com.hdfc.cards.credit.offers.dto.ClePromocodeServiceResponse;
import com.hdfc.cards.credit.offers.entity.ClePromocodeInfo;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.SystemException;
import com.hdfc.cards.credit.offers.repo.CleRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class  CreditLimitEnhanceServiceImpl implements CreditLImitEnhanceServiceI {

    final Logger serviceLogger = Logger.getLogger(CreditLimitEnhanceServiceImpl.class);
    @Autowired
    ClePromocodeServiceResponse clePromocodeServiceResponse;
   @Autowired
  private CleRepository cleRepository;

    @Override
    public ClePromocodeServiceResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException {

        // PowerMockito.whenNew(CreditLimitEnhanceDaoImpl.class).withNoArguments().thenReturn(creditLimitEnhanceDaoMock);
        //System.out.println("Entered into service layer verifyPromocode{}");
        serviceLogger.debug("Entered into service layer verifyPromocode{}");
        // call to dao layer and get clePromocodeInfo
        ClePromocodeInfo clePromocodeInfo = cleRepository.getOne(promocde);

        System.out.println("anilllllllllllllllll.........."+clePromocodeInfo);

        clePromocodeServiceResponse.setCurrentLimit(clePromocodeInfo.getCurrentLimit());
        clePromocodeServiceResponse.setEligibleAmount(clePromocodeInfo.getEligibleAmount());
        clePromocodeServiceResponse.setExpDate(clePromocodeInfo.getExpDate());
        clePromocodeServiceResponse.setPromocode(clePromocodeInfo.getPromocode());

        serviceLogger.debug("Exiting  from service layer verifyPromocode{}");
        //System.out.println("Exiting  from service layer verifyPromocode{}");


        return clePromocodeServiceResponse;
    }
}
