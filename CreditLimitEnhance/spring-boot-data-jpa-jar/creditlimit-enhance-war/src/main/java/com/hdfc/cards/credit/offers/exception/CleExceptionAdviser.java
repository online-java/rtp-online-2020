package com.hdfc.cards.credit.offers.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class CleExceptionAdviser {

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<CleErrorResponse> handleGlobalException(Throwable exception) {

        System.out.println("enterd into handle global exception");

        if (exception instanceof BusinessException) {
            System.out.println("enterd into  business  exception if block{}");
            BusinessException businessException = (BusinessException) exception;

            CleErrorResponse CleErrorResponse = prepareCleErrorResponse(businessException.getErrorCode(),
                    businessException.getErrorMessage());

            return ResponseEntity.status(HttpStatus.CONFLICT).body(CleErrorResponse);
        }
        if (exception instanceof EntityNotFoundException) {
            System.out.println("enterd into  CleDataAccessException   if block{}");
            exception.printStackTrace();
            CleDataAccessException cleDataAccessException = new CleDataAccessException(CleEnum.PROMOCODE_DETAILS_NOT_FOUND);

            CleErrorResponse CleErrorResponse = prepareCleErrorResponse(cleDataAccessException.getErrorCode(),
                    cleDataAccessException.getErrorMessage());

            return ResponseEntity.status(HttpStatus.CONFLICT).body(CleErrorResponse);
        }

        return null;

    }

    private CleErrorResponse prepareCleErrorResponse(String errorCode, String errorMessage) {

        CleErrorResponse CleErrorResponse = new CleErrorResponse();
        CleErrorResponse.setErrorCode(errorCode);
        CleErrorResponse.setErrorMessage(errorMessage);

        return CleErrorResponse;

    }

}

