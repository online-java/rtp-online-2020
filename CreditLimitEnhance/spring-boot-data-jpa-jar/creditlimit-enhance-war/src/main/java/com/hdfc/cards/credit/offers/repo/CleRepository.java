package com.hdfc.cards.credit.offers.repo;


import com.hdfc.cards.credit.offers.entity.ClePromocodeInfo;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

@Profile("spring-data-jpa")
public interface CleRepository extends JpaRepository<ClePromocodeInfo, String> {

}
