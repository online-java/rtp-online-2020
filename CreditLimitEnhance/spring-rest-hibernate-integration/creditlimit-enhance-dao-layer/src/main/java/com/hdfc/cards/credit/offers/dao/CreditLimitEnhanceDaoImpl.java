package com.hdfc.cards.credit.offers.dao;

import com.hdfc.cards.credit.offers.dao.dto.ClePromocodeDaoResponse;
import com.hdfc.cards.credit.offers.dao.entity.ClePromocodeInfo;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.SystemException;
import org.apache.log4j.Logger;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CreditLimitEnhanceDaoImpl implements CreditLimitEnhanceDaoI {
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private ClePromocodeDaoResponse clePromocodeDaoResponse;


    @Override
    public ClePromocodeDaoResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException {
        final Logger daoLogger = Logger.getLogger(CreditLimitEnhanceDaoImpl.class);
        //System.out.println("Entered into dao layer verifyPromocode{}");
        daoLogger.debug("Entered into DAO layer verifyPromocode{}");

        //call to Database and get thee ressponse


       Session session = sessionFactory.openSession();
       /* ClePromocodeInfo clePromocodeInfo = (ClePromocodeInfo) session.get(ClePromocodeInfo.class,promocde);
*/
        String promocodeQuery = "from ClePromocodeInfo cle where cle.promocode=:promocodeColumn";
                             /**  select ClePromocodeInfo cle where cle.promocode=?**/


       Query query = session.createQuery(promocodeQuery);
       query.setParameter("promocodeColumn",promocde);

        ClePromocodeInfo clePromocodeInfo = (ClePromocodeInfo) query.uniqueResult();

        clePromocodeDaoResponse.setCurrentLimit(clePromocodeInfo.getCurrentLimit());
        clePromocodeDaoResponse.setEligibleAmount(clePromocodeInfo.getEligibleAmount());
        clePromocodeDaoResponse.setExpDate(clePromocodeInfo.getExpDate());
        clePromocodeDaoResponse.setPromocode(clePromocodeInfo.getPromocode());



        daoLogger.debug("Exiting from DAO layer verifyPromocode{}");
        return clePromocodeDaoResponse;
    }

    public static void main(String[] args) throws CleDataAccessException, SystemException {

        CreditLimitEnhanceDaoImpl creditLimitEnhanceDao = new CreditLimitEnhanceDaoImpl();
        System.out.println(creditLimitEnhanceDao.verifyPromocode("hdfc2020"));

    }
}
