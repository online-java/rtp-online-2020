package com.hdfc.cards.credit.offers.dao.util;

import com.hdfc.cards.credit.offers.dao.entity.ClePromocodeInfo;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory;

    static {

        Configuration configuration = new Configuration();
        // it will load the hibernate configuration file
        configuration.configure("hibernate.cfg.xml").addAnnotatedClass(ClePromocodeInfo.class);// hibernate.cfg.xml
        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}