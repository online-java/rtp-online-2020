package com.hdfc.cards.credit.offers.dao;

import com.hdfc.cards.credit.offers.dao.dto.ClePromocodeDaoResponse;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.SystemException;

public interface CreditLimitEnhanceDaoI {
	//added comments
    public ClePromocodeDaoResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException;
}
