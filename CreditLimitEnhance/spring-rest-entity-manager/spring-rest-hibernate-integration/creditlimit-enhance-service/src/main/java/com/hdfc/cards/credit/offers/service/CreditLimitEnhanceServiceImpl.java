package com.hdfc.cards.credit.offers.service;

import com.hdfc.cards.credit.offers.dao.CreditLimitEnhanceDaoI;
import com.hdfc.cards.credit.offers.dao.CreditLimitEnhanceDaoImpl;
import com.hdfc.cards.credit.offers.dao.dto.ClePromocodeDaoResponse;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.SystemException;
import com.hdfc.cards.credit.offers.service.dto.ClePromocodeServiceResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class  CreditLimitEnhanceServiceImpl implements CreditLImitEnhanceServiceI {
    @Autowired
    private CreditLimitEnhanceDaoI creditLimitEnhanceDaoI;
    final Logger serviceLogger = Logger.getLogger(CreditLimitEnhanceServiceImpl.class);
    @Autowired
    ClePromocodeServiceResponse clePromocodeServiceResponse;


    @Override
    public ClePromocodeServiceResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException {

        // PowerMockito.whenNew(CreditLimitEnhanceDaoImpl.class).withNoArguments().thenReturn(creditLimitEnhanceDaoMock);
        //System.out.println("Entered into service layer verifyPromocode{}");
        serviceLogger.debug("Entered into service layer verifyPromocode{}");
        // call to dao layer and get ClePromocodeDaoResponse
        ClePromocodeDaoResponse clePromocodeDaoResponse = creditLimitEnhanceDaoI.verifyPromocode(promocde);


        clePromocodeServiceResponse.setCurrentLimit(clePromocodeDaoResponse.getCurrentLimit());
        clePromocodeServiceResponse.setEligibleAmount(clePromocodeDaoResponse.getEligibleAmount());
        clePromocodeServiceResponse.setExpDate(clePromocodeDaoResponse.getExpDate());
        clePromocodeServiceResponse.setPromocode(clePromocodeDaoResponse.getPromocode());

        serviceLogger.debug("Exiting  from service layer verifyPromocode{}");
        //System.out.println("Exiting  from service layer verifyPromocode{}");

        return clePromocodeServiceResponse;
    }
}
