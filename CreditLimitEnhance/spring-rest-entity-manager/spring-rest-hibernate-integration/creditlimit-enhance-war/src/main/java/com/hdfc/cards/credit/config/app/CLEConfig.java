package com.hdfc.cards.credit.config.app;

import com.hdfc.cards.credit.offers.dao.config.CLEDaoConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.GenericServlet;

@Configuration
@ComponentScan({"com.hdfc.cards.credit.*"})
@Import({CLEDaoConfig.class})
@EnableWebMvc
public class CLEConfig {

   private GenericServlet genericServlet;
}
