package com.hdfc.cards.credit.offers.dao;

import com.hdfc.cards.credit.offers.dao.dto.ClePromocodeDaoResponse;
import com.hdfc.cards.credit.offers.dao.entity.ClePromocodeInfo;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.SystemException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CreditLimitEnhanceDaoImpl implements CreditLimitEnhanceDaoI {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ClePromocodeDaoResponse clePromocodeDaoResponse;


    @Override
    public ClePromocodeDaoResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException {
        final Logger daoLogger = Logger.getLogger(CreditLimitEnhanceDaoImpl.class);
        //System.out.println("Entered into dao layer verifyPromocode{}");
        daoLogger.debug("Entered into DAO layer verifyPromocode{}");

        //call to Database and get thee ressponse


        ClePromocodeInfo clePromocodeInfo = (ClePromocodeInfo)  entityManager.find(ClePromocodeInfo.class,promocde);

        clePromocodeDaoResponse.setCurrentLimit(clePromocodeInfo.getCurrentLimit());
        clePromocodeDaoResponse.setEligibleAmount(clePromocodeInfo.getEligibleAmount());
        clePromocodeDaoResponse.setExpDate(clePromocodeInfo.getExpDate());
        clePromocodeDaoResponse.setPromocode(clePromocodeInfo.getPromocode());



        daoLogger.debug("Exiting from DAO layer verifyPromocode{}");
        return clePromocodeDaoResponse;
    }

    public static void main(String[] args) throws CleDataAccessException, SystemException {

        CreditLimitEnhanceDaoImpl creditLimitEnhanceDao = new CreditLimitEnhanceDaoImpl();
        System.out.println(creditLimitEnhanceDao.verifyPromocode("hdfc2020"));

    }
}
